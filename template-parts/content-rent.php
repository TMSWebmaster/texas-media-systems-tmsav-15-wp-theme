<?php
/**
 * Template part for displaying posts.
 *
 * @package TMSAV-15
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

		<!-- try it -Clint -->
			<div id="rentnav">
    <ul>
        <li><a href="http://texasmediasystems.com/rentals" title="TMS Rental Policies">Rental Policies</a></li>
		<li><a href="http://texasmediasystems.com/rentals/camcorder-rentals/" title="TMS Camcorder Rentals">Camcorders</a></li>
        <li><a href="http://texasmediasystems.com/rentals/lens-rentals/" title="TMS Lens Rentals">Lenses</a></li>
        <li><a href="http://texasmediasystems.com/rentals/support/" title="TMS Camera Support Rentals">Support</a></li>
		<li><a href="http://texasmediasystems.com/rentals/decksmedia/" title="TMS Decks & Media Rentals">Decks/Media</a></li>
		<li><a href="http://texasmediasystems.com/rentals/presentation/" title="TMS Presentation Rentals">Presentation</a></li>
		<li><a href="http://texasmediasystems.com/rentals/audio/" title="TMS Audio Equipment Rentals">Audio</a></li>
		<li><a href="http://texasmediasystems.com/rentals/converters/" title="TMS Converter Rentals">Converters</a></li>
		<li><a href="http://texasmediasystems.com/rentals/switchers/" title="TMS Switcher Rentals">Switchers</a></li>
		<li><a href="http://texasmediasystems.com/rentals/lighting/" title="TMS Lighting Rentals">Lighting</a></li>
		<li><a href="http://texasmediasystems.com/rentals/services/" title="Rental Packages & Services">Packages & Services</a></li>
    </ul>
</div>
<div class="clearit">&nbsp;</div>
	<hr class="nomarginsareyoukiddingme" />
						
						
						<!-- end 'try it' -Clint -->
		
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php texas_media_systems_theme_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content nomarginsareyoukiddingme">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'texas-media-systems-theme' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'texas-media-systems-theme' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php texas_media_systems_theme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
