<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package TMSAV-15
 */

?>

	</div><!-- #content -->
	</div><!-- end #clearFloats -->


</div><!-- end #container -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<div class="wrapper">

<div class="sweeper">
ph. <a style="color: #F8F2EC; text-decoration: none;" href="tel:5124401400">(512) 440-1400</a><br>
M-F | 9AM-6PM CST<br>
<a style="color: #F8F2EC; text-decoration: none;" href="http://texasmediasystems.com/sitemap_index.xml" title="XML Sitemap" target="_blank">XML Sitemap</a> | <a style="color: #F8F2EC; text-decoration: none;" href="http://texasmediasystems.com/sitemap/" title="HTML Sitemap" target="_blank">HTML Sitemap</a>

</div><!-- end .sweeper -->

<p>
   
   <span>
     <span>4311 Medical Parkway</span><br>
	 <span>Austin</span>,  <span>TX&nbsp;</span><span> 78756</span>
</span><br>
Copyright © 2017 Texas Media Systems</p>

</div><!-- end .wrapper -->
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="application/ld+json">
{
 "@context":"http://schema.org",
 "@type":"LocalBusiness",
 "address":{
   "@type":"PostalAddress",
   "streetAddress":"4311 Medical Parkway",
   "addressLocality":"Austin",
   "addressRegion":"TX",
   "postalCode":"78756",
   "telephone":"(512) 440-1400"
    },         
   "openingHoursSpecification":{
     "@type":"OpeningHoursSpecification",     
     "dayOfWeek":"Monday to Saturday",
     "opens":"09:00",
     "closes":"18:00"
    },    
      "name":"Texas Media Systems",
      "url":"http://texasmediasystems.com/",
      "sameAs":[
      "http://www.facebook.com/texasmediasystems",
      "http://twitter.com/txmedia",
      "http://pinterest.com/txmedia",
      "http://vimeo.com/txmedia"
    ]
}
</script>

</body>
</html>
